# README

## Installation

`bundle`

`yarn`

`cp config/database.yml.example config/database.yml` (and update)

`cp .env.example .env` (and update)

`rake db:create`

`rake db:migrate`

`rake db:seed`

(Install [overmind](https://github.com/DarthSim/overmind) which is like foreman to run rails server and webpack-dev-server)

You can run the app (with webpack-dev-server) using:

`overmind start -f Procfile.dev`

(To debug, in another terminal tab use `overmind connect server`)

Or if you're not doing front-end development:

`rails server`

### Front-End

This app doesn't use the Rails asset pipeline. Front-end code can be found in: `./frontend/`.

This is inspired by: https://evilmartians.com/chronicles/evil-front-part-1

## Backups

There is an automated cron job + rake task that does a db dump and uploads that to AWS S3 on the daily.

### Restoring

1. Go into AWS S3 bucket and download the .tar file for the environment + date you want.
2. `cp ~/Downloads/db_backup.tar ./backups/2017.09.25-staging.tar`
3. `tar -xvf backups/2017.09.25-staging.tar`
4. `gunzip db_backup/databases/PostgreSQL.sql.gz`
5. `rake db:drop` (`rdd`)
6. `rake db:create` (`rdc`)
7. `psql -d people-speak-dev -f ./backups/db_backup/databases/PostgreSQL.sql`
