class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound,
              ActionView::MissingTemplate,
              :with => :render_404 unless Rails.env.development? || Rails.env.test?

  before_action :set_paper_trail_whodunnit
  before_action :set_raven_context

  def render_404
    render :template => "pages/404", :formats => [:html], :status => 404
  end

  private

  def set_raven_context
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
    Raven.user_context(admin: current_admin_user.email) if current_admin_user.present?
  end

end
