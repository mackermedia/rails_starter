require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsStarter
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.autoload_paths << Rails.root.join("lib")
    config.enable_dependency_loading = true

    config.action_mailer.deliver_later_queue_name = 'default'

    config.generators do |g|
      g.system_tests nil
      g.test_framework :rspec
      g.stylesheets false
      g.javascripts false
      g.helper false
      g.helper_specs false
      g.assets false
      g.controller_specs false
      g.channel assets: false
    end
  end
end
