# encoding: utf-8

require 'yaml'
require 'dotenv'

rails_env       = ENV['RAILS_ENV'] || 'development'
database_yml    = File.expand_path('../../../database.yml', __FILE__)
db_config       = YAML.load_file(database_yml)[rails_env]
Dotenv.load

## RESTORING BACKUP!!!
# bundle exec rake db:drop db:create
# tar -xvf /tmp/db_backup.tar -C /tmp && gunzip -c /tmp/db_backup/databases/PostgreSQL.sql.gz | psql -U :user :database

##
# Backup Generated: my_backup
# Once configured, you can run the backup with the following command:
#
# $ backup perform -t db_backup [-c <path_to_configuration_file>]
#
# For more information about Backup's components, see the documentation at:
# http://backup.github.io/backup
#
Model.new(:db_backup, "Database backup for #{ENV['RAILS_ENV']}") do

  database PostgreSQL do |db|
    db.name      = db_config['database']
    db.username  = db_config['user']
    db.password  = db_config['password']
    db.host      = 'localhost'
  end

  ##
  # Amazon Simple Storage Service [Storage]
  #
  store_with S3 do |s3|
    # AWS Credentials
    s3.access_key_id     = ENV['AWS_ACCESS_KEY_ID']
    s3.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
    s3.region            = ENV['AWS_REGION']
    s3.bucket            = ENV['AWS_BUCKET']
    s3.path              = "backups/#{rails_env}"
    s3.fog_options       = { path_style: true }
  end

  compress_with Gzip

  ##
  # Mail [Notifier]
  #
  # The default delivery method for Mail Notifiers is 'SMTP'.
  # See the documentation for other delivery options.
  #
  notify_by Slack do |slack|
    slack.on_success           = true
    slack.on_warning           = true
    slack.on_failure           = true

    # TODO: figure out how to inject this from Rails env into deploy -> cron -> backup task
    slack.webhook_url = 'https://hooks.slack.com/services/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'
  end

end
