if %w(development test).include?(Rails.env)
  ActiveSupport.on_load(:active_record) do
    Que.mode = Rails.env.test? ? :sync : :async
  end

  Que.log_formatter = proc do |data|
    unless data[:event] == :job_unavailable
      JSON.dump(data)
    end
  end
end
