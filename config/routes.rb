Rails.application.routes.draw do

  mount Lockup::Engine, at: '/lockup' if Rails.env.staging?

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: 'pages#home'

  match '(*path)', :via => :all, :to => 'application#render_404'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
