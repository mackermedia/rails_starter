module LoginHelpers

  def login_admin(admin_user = create(:admin_user))
    visit "/admin"

    fill_in :admin_user_email, with: admin_user.email
    fill_in :admin_user_password, with: admin_user.password
    click_on "Login"
  end

end
